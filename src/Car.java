
public class Car {
	
	String company="";
	String model="";
	int price=0;
	Radio r = new Radio();
	
	Car(String company, String model, int price){
		setup(company,model,price);
	}
	
	Car(){
		
	}
	
	public void start() {
		
		System.out.println("Started");
		
	}
	
	public void stop() {
		
		System.out.println("Stopped");
		
	}
	
	public void run() {
		
		System.out.println("Running");
		
	}
	
	public void setup(String company, String model, int price) {
		this.company=company;
		this.model=model;
		this.price=price;
	}
	
	public void display() {
		System.out.println("Company is:" +company);
		System.out.println("Model is: "+model);
		System.out.println("Price is: "+price);
		
		r.play(23);
	}
}
