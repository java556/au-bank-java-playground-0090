
public class Circle {
	int radius;
	
	Circle(int radius){
		setRadius(radius);
	}
	
	 public void setRadius(int radius) {
		 this.radius = radius;
	 }
	 
	 public void area() {
		 double area = Math.PI*Math.pow(radius,2);
		 System.out.println("Area of circle is : "+area);
	 }
	 
	 public void circumference() {
		 double circumference = 2*Math.PI*radius;
		 System.out.println("Circumference is : "+circumference);
	 }
}
