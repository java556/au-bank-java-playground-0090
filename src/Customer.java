
public class Customer {
	private String firstName;
	private String lastName;
	private int accuntNummber;
	private float bankBalance;
	private boolean active;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAccuntNummber() {
		return accuntNummber;
	}
	public void setAccuntNummber(int accuntNummber) {
		this.accuntNummber = accuntNummber;
	}
	public float getBankBalance() {
		return bankBalance;
	}
	public void setBankBalance(float bankBalance) {
		this.bankBalance = bankBalance;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
