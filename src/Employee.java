
public class Employee {
	private String name="";
	private int baseSalary;
	private int hourlyRates;
	static int count;
	
	Employee(String name, int baseSalary)
	{
		this(name,baseSalary,0);
	}
	
	Employee(String name,int baseSalary, int hourlyRates)
	{
		setName(name);
		setBaseSalary(baseSalary);
		setHourlyRates(hourlyRates);
		count++;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setBaseSalary(int baseSalary) {
		this.baseSalary = baseSalary;
	}
	
	public void setHourlyRates(int hourlyRates) {
		this.hourlyRates = hourlyRates;
	}
	
	public String getName(String name) {
		return name;
	}
	
	public int getBaseSalary() {
		return baseSalary;
	}
	
	public int getHourlyRates() {
		return hourlyRates;
	}
	
	public void wages(int Ehours)
	{
		int eWage = baseSalary + (hourlyRates*Ehours);
		System.out.println("Wage is : "+eWage);
	}
	
	
}
