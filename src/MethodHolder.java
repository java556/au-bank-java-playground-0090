
public class MethodHolder {
	
	public void display() {
		System.out.println("Welcome");
	}
	
	public void add(int a,int b) {
		 int c = a+b;
		 System.out.println(c);
	}
	
	public int getGST() {
		int gstRate=18;
		return gstRate;
	}
	
	public int square(int number) {
		int sq = number*number;
		return sq;
	}
}
