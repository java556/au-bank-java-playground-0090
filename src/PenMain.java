
public class PenMain {
	public static void main(String[] args)
	{
		Pen p=null; // create an object
		System.out.println(p);
		p=new Pen(); // Instantiated
		System.out.println(p);
		
		p.openCap();
		p.write();
		p.closeCap();
		
		System.out.println(p.color);
		System.out.println(p.inkLevel);
	}
}
