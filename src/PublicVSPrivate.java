
public class PublicVSPrivate {

	public int publicVariable=100;
	private int privateVariable=50;
	
	public int displayPrivateVariable() {
		System.out.println("Private : "+privateVariable);
		return privateVariable;
	}
	
}
