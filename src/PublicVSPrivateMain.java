
public class PublicVSPrivateMain {
	public static void main(String[] args) {
		
		PublicVSPrivate obj=new PublicVSPrivate();
		int a = obj.publicVariable;
		System.out.println("Public : "+a);
		int b = obj.displayPrivateVariable();
	}
}
