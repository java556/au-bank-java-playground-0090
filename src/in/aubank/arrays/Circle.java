package in.aubank.arrays;

public class Circle {
	int radius;
	
	public Circle(int radius) {
		// TODO Auto-generated constructor stub
		super();
		setRadius(radius);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	
}
