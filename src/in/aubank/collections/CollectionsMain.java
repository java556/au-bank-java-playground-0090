package in.aubank.collections;

import java.util.*;

import javax.print.attribute.standard.MediaSize.Other;

public class CollectionsMain {
	
	public static void main(String[] args) {
		
		Collection<String> collection=new ArrayList<String>();
		collection.add("a");
		collection.add("b");
		collection.add("c");
		
		System.out.println(collection);
		
		Collections.addAll(collection, "e","f","g");
		
		System.out.println(collection);
		
		int size=collection.size();
		System.out.println("Size of collection: "+size);
		
		collection.remove("c");
		System.out.println(collection);
		
		collection.clear();
		System.out.println("Empty Status: "+collection.isEmpty());
		
		Object[] objectArray=collection.toArray();
		String[] stringArray=collection.toArray(new String[0]);
		
		collection=new ArrayList<String>();
		collection.add("a");
		collection.add("b");
		collection.add("c");
		
		Collection other=new ArrayList<String>();
		other.add("a");
		other.add("b");
		other.add("c");
		
		System.out.println(collection==other);
		System.out.println(collection.equals(other));
		
		
	}
	
}
