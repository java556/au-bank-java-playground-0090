package in.aubank.collections;

public class Customer implements Comparable<Customer> {
	private int id;
	private String name;
	private String email;
	private float balance;
	
	public Customer() {
		// TODO Auto-generated constructor stub
		super();
	}

	public Customer(int id, String name, String email, float balance) {
		super();
		setId(id);
		setName(name);
		setEmail(email);
		setBalance(balance);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", balance=" + balance + "]";
	}

	@Override
	public int compareTo(Customer other) {
		// TODO Auto-generated method stub
		
		return this.getId()-other.getId();
	}
	
	
	
	
}
