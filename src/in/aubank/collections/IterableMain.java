package in.aubank.collections;
import java.util.ArrayList;
import java.util.Iterator;

public class IterableMain {
	
	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<>(10);
		list.add("A");
		list.add("B");
		list.add("C");
		
		for(String s:list) {
			System.out.println(s);
		}
		
		System.out.println("*-------------iterator value---------------*");
		
		Iterator<String> iterator=list.iterator();
		
		while(iterator.hasNext()) {
			String elementString=iterator.next();
			System.out.println(elementString.toLowerCase());
		}
		
	}
	
}
