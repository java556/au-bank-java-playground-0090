package in.aubank.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListMain {

	public static void main(String[] args) {
		
		List<String> list=new ArrayList<String>();
		
		list.add("a");
		list.add("b");
		list.add("c");
		
		System.out.println(list);
		
		list.add(0,"!");
		System.out.println(list);
		
		Collections.addAll(list, "x","y","z","A","a");
		System.out.println(list);
		
		String first=list.get(0);
		System.out.println("first Element: "+first);
		
		list.set(0, "#");
		System.out.println(list);
		
		list.remove(0);
		System.out.println(list);
		
		int firstIndex = list.indexOf("a");
		System.out.println("Index of A is: "+firstIndex);
		
		int lastIndex = list.lastIndexOf("a");
		System.out.println("Last Index of A is: "+lastIndex);
		
	}
}
