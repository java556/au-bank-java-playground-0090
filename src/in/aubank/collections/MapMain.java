package in.aubank.collections;

import java.awt.MenuComponent;
import java.util.HashMap;
import java.util.Map;

public class MapMain {
	public static void main(String[] args) {
		
		Customer c1=new Customer(1,"TOM","tom@gmailcom",2345.87F);
		Customer c2=new Customer(2,"ELI","eli@gmailcom",734985.87F);
		Customer c3=new Customer(3,"ALEX","alex@gmailcom",43345.87F);
		Customer c4=new Customer(4,"LILY","lily@gmailcom",43395.87F);
		
		Map<String, Customer> map=new HashMap<String, Customer>();
		map.put(c1.getEmail(), c1);
		map.put(c2.getEmail(), c2);
		map.put(c3.getEmail(), c3);
		map.put(c4.getEmail(), c4);
		
		System.out.println(map);
		
		boolean exists=map.containsKey("lily@gmail.com");
		System.out.println(exists);
		
		Customer c=map.get("tom@gmail.com");
		System.out.println(c);
		
		Customer other=map.get("tommy@gmail.com");
		System.out.println(other);
		
		Customer defaultCustomer=new Customer(0,"","",0.0F);
		other=map.getOrDefault("tommy@gmail.com", defaultCustomer);
		System.out.println(other);
		
		map.replace("tommy@gmail.com", new Customer(1,"Thomas","thomas@gmail.com",7987.98F));
		
		System.out.println(map);
		
		for(String key:map.keySet()) {
			System.out.println(key);
		}
		
		for(Customer value:map.values()) {
			System.out.println(value);
		}
		
		
		
		
		
	}
}
