package in.aubank.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SetMain {

	public static void main(String[] args) {
		
		Set<String> set=new HashSet<>();
		set.add("alpha");
		set.add("bravo");
		set.add("charlie");
		set.add("delta");
		set.add("alpha");
		
		Iterator<String> iterator = set.iterator();
		
		while(iterator.hasNext()) {
			String element=iterator.next();
			System.out.println(element);
		}
		
		String[] airportCodes = {"BOM","JAI","BLR","MAA","PNQ","BOM","BLR"};
		List<String> airportCodesList=Arrays.asList(airportCodes);
//		Set<String> setofAirportCode=new HashSet<>();
		
		Set<String> setofAirportCode=new TreeSet<>(airportCodesList);
		
		System.out.println("O: "+airportCodesList);
		System.out.println("S: "+setofAirportCode);
		
		Set<String> set1=new HashSet<>();
		set1.add("alpha");
		set1.add("bravo");
		set1.add("charlie");
		set1.add("delta");
		set1.add("alpha");
		
		Set<String> set2=new HashSet<>();
		set2.add("alpha");
		set2.add("elephant");
		set2.add("foxtrot");
		
//		System.out.println(set1.addAll(set2));
//		System.out.println(set1);
		
		set1.retainAll(set2);
		System.out.println(set1);
	}
	
}
