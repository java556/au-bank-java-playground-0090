package in.aubank.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class UtilityMain {
	
	public static void main(String[] args) {
		
		List<String> list=new ArrayList<>(Arrays.asList("a","c","d","b","e","z","y","x"));
		System.out.println(list);
		
		Collections.sort(list);
		System.out.println("After Sorting: "+list);
		
		Collections.reverse(list);
		System.out.println("After Reverse Sorting: "+list);
		
		Set<Integer> numbers=new TreeSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9));
		System.out.println("Max Value: "+Collections.max(numbers));
		System.out.println("Min Value: "+Collections.min(numbers));
		
		Customer c1=new Customer(1,"TOM","tom@gmailcom",2345.87F);
		Customer c2=new Customer(2,"ELI","eli@gmailcom",734985.87F);
		Customer c3=new Customer(3,"ALEX","alex@gmailcom",43345.87F);
		Customer c4=new Customer(4,"LILY","lily@gmailcom",43395.87F);
		
		List<Customer> customers=new ArrayList<Customer>();
		customers.add(c1);
		customers.add(c2);
		customers.add(c3);
		customers.add(c4);
		
		printCustomer(customers);
		
		Collections.sort(customers);
		printCustomer(customers);
		
		Collections.sort(customers,new NameComparator());
		printCustomer(customers);
		
		Collections.sort(customers, (o1,o2)->(int)(o1.getBalance()-o2.getBalance()));
	}
	
	private static void printCustomer(List<Customer> list) {
		// TODO Auto-generated method stub
		System.out.println("------------*-------------");
		
		for(Customer c:list) {
			System.out.println(c);
		}
	}

	
}
