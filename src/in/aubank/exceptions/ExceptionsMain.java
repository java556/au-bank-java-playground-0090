package in.aubank.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionsMain {
	public static void main(String[] args) {
		FileReader fr = null;
		try {
			
		int[] numbers = {1,2,3,4,5};
		
		System.out.println(numbers[2]);
		System.out.println(numbers[8]);
		
		int i = 10/0;
		
			 fr=new FileReader("abc.txt");
		}
		catch(ArithmeticException e) {
			System.out.println("--------ATE-----------");
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println("------------IOE-----------");
			e.printStackTrace();
		}		
		catch(Exception e) {
			System.out.println("File not found");
			e.printStackTrace();
		}
		finally {
			System.out.println("no ex occurs");
			System.out.println("ex occurs");
			if(fr!=null) {
				try {
					fr.close();
					System.out.println("File Reader closed");
				}
				catch (IOException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
				
		}
		
		System.out.println("Execution reaches here ........");
	}
}
