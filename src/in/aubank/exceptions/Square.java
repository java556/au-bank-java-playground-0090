package in.aubank.exceptions;

public class Square {
	int length;
	
	public void setLength(int length) throws Exception{
		
		if(length>0) {
			this.length=length;
		}
		else {
			throw new SquareException("length should be greater than 0");
		}
		
	}
	
	public void area() {
		int area=length*length;
		System.out.println("Area for the square with length "+length+" is "+ area);
	}
}
