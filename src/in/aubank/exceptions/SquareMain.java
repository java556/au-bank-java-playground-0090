package in.aubank.exceptions;

public class SquareMain {
	public static void main(String[] args) {
		try {
		Square sq = new Square();
		sq.setLength(10);
		sq.area();
		
		Square sq1 = new Square();
		sq1.setLength(-9);
		sq1.area();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
