package in.aubank.exceptions;
import java.util.Scanner;
public class Voter {
	private String name;
	private String ch;
	private int age;
	private final String nationality = "Indian";
	
	public Voter(String name, String ch, int age) throws Exception{
		setName(name);
		setCh(ch);
		if(age>18 && age<120)
		{
			setAge(age);
		}
		else {
			throw new VoterException("Age must be in between 18 - 120");
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCh() {
		return ch;
	}
	public void setCh(String ch) {
		this.ch = ch;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getNationality() {
		return nationality;
	}
	
	public void show() {
		System.out.println("Hi "+name+", you are "+ch+", you are "+age+" years old, you are "+nationality+" \n you are eligible for voting");
	}
	
}
