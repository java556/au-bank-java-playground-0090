package in.aubank.exceptions;
import java.util.Scanner;
public class VoterMain {
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Enter your name: ");
			String name=sc.nextLine();
			
			System.out.println("Enter your gender: ");
			String ch = sc.next();
			
			System.out.println("Enter your age: ");
			int age = sc.nextInt();
			
			Voter v1 = new Voter(name, ch, age);
			
			v1.show();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
