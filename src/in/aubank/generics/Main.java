package in.aubank.generics;

import java.util.Date;

import in.aubank.arrays.Circle;

public class Main {
	
	public static void main(String[] args) {
		
		Box intBox = new Box();
		
		intBox.add(10);
		intBox.add(20);
		intBox.add(30);
		//intBox.add("AU Bank");
		//intBox.add(new java.util.Date());
		
		Integer i= (Integer)intBox.get(0);
		System.out.println(i);
		
		String i3= (String)intBox.get(3);
		System.out.println(i3);
		
		MyBox<String> stringBox=new MyBox<>();
		
		stringBox.add("A");
		stringBox.add("B");
		
		String s=stringBox.get(1);
		
		MyBox<Circle> circleBox=new MyBox<Circle>();
		
		
	}
	
}
