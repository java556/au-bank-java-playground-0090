package in.aubank.inheritance;

public class CheckBox extends UIControl{
	
	boolean selected;
	
	public CheckBox() {
		super(true);
	}
	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Override
	public void render() {
		System.out.println("CheckBox rendered");
	}
	
}
