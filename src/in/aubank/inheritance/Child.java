package in.aubank.inheritance;

public class Child extends Parent {
	
	public void m3() {
		System.out.println("Child m3");
	}
	
	public void m4() {
		System.out.println("Child m4");
	}
	
}
