package in.aubank.inheritance;

public class InheritanceMain {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.m1();
		child.m2();
		child.m3();
		child.m4();
		
		Parent parent=new Parent();
		
		System.out.println("Super Class for Child: "+
				child.getClass().getSuperclass().getName());
		
		System.out.println("Super Class for Parent: "+
				parent.getClass().getSuperclass().getName());
		
		System.out.println("*------------------------*");
		
		C c = new C();
		
		
	}
}
