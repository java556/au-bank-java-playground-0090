package in.aubank.inheritance;

public class TextBox extends UIControl{
	
	String text;
	
	public TextBox(boolean tr) {
		// TODO Auto-generated constructor stub
		super(tr);
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	
	public void clear() {
		text="";
	}
	
	public void render() {
		System.out.println("TextBox rendered");
	}
	
}
