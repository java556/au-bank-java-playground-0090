package in.aubank.inheritance;

abstract public class UIControl {
	
	boolean enabled;
	
	public UIControl(boolean enabled) {
		this.enabled = enabled;
	}
	
	public void enable() {
		enabled = true;
	}
	
	public void disable() {
		enabled = false;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public abstract void render(); //{
		//System.out.println("UI rendered");
	//}
}
