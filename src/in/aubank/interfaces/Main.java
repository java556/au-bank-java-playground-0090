package in.aubank.interfaces;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter your name: ");
		
		String name=scanner.nextLine();
		System.out.println("Enter Your Taxable Income: ");
		double taxableIncome=scanner.nextDouble();
		System.out.println("Year for which tax is to be calculated [2020,2021,2022]: ");
		int year=scanner.nextInt();
		
		switch (year) {
		case 2020:
			TaxCalculator2020 calculator20=new TaxCalculator2020(taxableIncome);
			TaxReport report20=new TaxReport(calculator20);
			System.out.println("Hey "+name+" your tax for "+year+" is ");
			report20.show();
			break;
			
		case 2021:
			TaxCalculator2021 calculator21=new TaxCalculator2021(taxableIncome);
			TaxReport report21=new TaxReport(calculator21);
			System.out.println("Hey "+name+" your tax for "+year+" is ");
			report21.show();
			break;
			
		case 2022:
			TaxCalculator2022 calculator22=new TaxCalculator2022(taxableIncome);
			TaxReport report22=new TaxReport(calculator22);
			System.out.println("Hey "+name+" your tax for "+year+" is ");
			report22.show();
			break;

		default:
			System.err.println("NA");
			break;
		}
	}
}
