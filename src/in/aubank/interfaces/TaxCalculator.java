package in.aubank.interfaces;

public interface TaxCalculator {
	
	
	public double calculateTax();
	
}
