package in.aubank.interfaces;

public class TaxCalculator2020 implements TaxCalculator{

	double taxableIncome;
	
	public TaxCalculator2020(double taxableIncome) {
		// TODO Auto-generated constructor stub
		this.taxableIncome = taxableIncome;
	}
	
	@Override
	public double calculateTax() {
		return taxableIncome*0.25;
	}
}
