package in.aubank.interfaces;

public class TaxReport {
	
	TaxCalculator calculator;
	
	public TaxReport(TaxCalculator calculator) {
		this.calculator = calculator;
	}
	
	public void show() {
		double tax=calculator.calculateTax();
		System.out.println("Tax on your income is "+tax);
	}
}
