package in.aubank.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BoilerPlate2 {

public static void main(String[] args) {
		
		String url="jdbc:postgresql://localhost:5432/au-bank-sept-2022";
		String username="postgres";
		String password ="An@s.1999"; 
		
		Connection con=null;
		try {
			Class.forName("org.postgresql.Driver");
			System.out.println("Driver loaded");
			
			con=DriverManager.getConnection(url,username,password);
			System.out.println("Connection established..");
			
			String query="create table demo(id integer, name text)";
			PreparedStatement pstmt = con.prepareStatement(query);
			
			boolean result=pstmt.execute();
			
			if(!result) {
				System.out.println("Table created successfully");
			}
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if(con!=null) {
				try {
					con.close();
				}
				catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		
	}
}
