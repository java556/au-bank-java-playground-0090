package in.aubank.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteCustomer {
	public static void main(String[] args) {
		
		String url="jdbc:postgresql://localhost:5432/au-bank-sept-2022";
		String username="postgres";
		String password ="An@s.1999"; 
		
		Connection con=null;
		try {
			Class.forName("org.postgresql.Driver");
			System.out.println("Driver loaded");
			
			con=DriverManager.getConnection(url,username,password);
			System.out.println("Connection established..");
			
			int id=1;
			String query="Delete from customer where id=?";
			
			
			
			
			PreparedStatement pstmt = con.prepareStatement(query);
			pstmt.setInt(1, id);
			
			
			
			int rowsDeleted=pstmt.executeUpdate();
			
			
			if(rowsDeleted!=0) {
				System.out.println("Table deleted successfully");
			}
			
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if(con!=null) {
				try {
					con.close();
				}
				catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		
	}
}
