package in.aubank.jdbc;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Scanner;

public class InsertCustomer {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {		
		String url="jdbc:postgresql://localhost:5432/au-bank-sept-2022";
		String username="postgres";
		String password ="An@s.1999"; 
		
		Connection con=null;
		try {
			Class.forName("org.postgresql.Driver");
			System.out.println("Driver loaded");
			
			con=DriverManager.getConnection(url,username,password);
			System.out.println("Connection established..");
			
			String name=getValueFromConsole("Enter Name: ");
			String email=getValueFromConsole("Enter email: ");
			String phone=getValueFromConsole("Enter phone: ");
			
			String query="Insert into " + "customer(name,email,phone,date_of_creation) "+"values (?,?,?,?)";
			PreparedStatement pstmtPreparedStatement=con.prepareStatement(query);
			
			pstmtPreparedStatement.setString(1, name);
			pstmtPreparedStatement.setString(2, email);
			pstmtPreparedStatement.setString(3, phone);
			pstmtPreparedStatement.setObject(4, LocalDate.now());
			
			int rowsInserted=pstmtPreparedStatement.executeUpdate();
			System.out.println("No. of records created: "+rowsInserted);
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if(con!=null) {
				try {
					con.close();
				}
				catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		
	}

	public static String getValueFromConsole(String prompt) {
		System.out.println(prompt);
		String valueString=sc.next();
		return valueString;
		
	}
}
