package in.aubank.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectSingleCustomer {
public static void main(String[] args) {
		
		String url="jdbc:postgresql://localhost:5432/au-bank-sept-2022";
		String username="postgres";
		String password ="An@s.1999"; 
		
		Connection con=null;
		try {
			Class.forName("org.postgresql.Driver");
			System.out.println("Driver loaded");
			
			con=DriverManager.getConnection(url,username,password);
			System.out.println("Connection established..");
			
			
			int id=1 ;
			String query="select name,email,phone,date_of_creation,id from customer where id=?";
			PreparedStatement pstmt = con.prepareStatement(query);
			
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()) {
				String name = rs.getString("name");
				String email = rs.getString("email");
				String phone = rs.getString("phone");
				String dateOfCreation = rs.getString("date_of_creation");
				
				System.out.println(name+" | "+email+" | "+phone+" | "+" | "+dateOfCreation);
			
			
			}
			
				
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if(con!=null) {
				try {
					con.close();
				}
				catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		
	}
}
