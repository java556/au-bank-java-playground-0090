package in.aubank.lambdas;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
public class ConsumerMain {
	public static void main(String[] args) {
		
		List<Integer> numbers=new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7));
		
		for(Integer i:numbers) {
			System.out.println(i);
		}
		
		numbers.forEach(number->System.out.println(number));
		
		numbers.forEach(System.out::println);
		
		System.out.println("--------------------------------------------------------------");
		
		List<String> alphabets=new ArrayList<String>(Arrays.asList("a","b","c","d"));
		
		Consumer<String> print=(word)->System.out.println(word);
		Consumer<String> printUpperCase=(word)->System.out.println(word.toUpperCase());
		
		alphabets.forEach(print.andThen(printUpperCase));
	}
}
