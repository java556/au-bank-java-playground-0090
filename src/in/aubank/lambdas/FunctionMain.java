package in.aubank.lambdas;

import java.util.function.Function;

public class FunctionMain {
	
	public static void main(String[] args) {
		Function<String,Integer> map=(word)->word.length();
		int l = map.apply("AU Bank");
		System.out.println(l);
		
		Function<String, String> replaceEquals=(input)->input.replace("=", ":");
		Function <String,String> addCurlyBracesFunction=(input)->"{"+input+"}";
		
		String check=replaceEquals.andThen(addCurlyBracesFunction).apply("Mumbai=MH");
		System.out.println(check);
	}
}
