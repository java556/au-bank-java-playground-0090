package in.aubank.lambdas;

import java.security.MessageDigest;

public class Main {
	public static void show() {
		Printer p = new ConsolePrinter();
		greet(p);

		greet(new Printer() {
			@Override
			public void print(String message) {
				System.out.println(message.toUpperCase());
			}
		});
		
		greet((message)->{System.out.println(message.toLowerCase());});
		greet(message->{System.out.println(message.toLowerCase());});
		greet(message->System.out.println(message.toLowerCase()));
		Printer printer =message->System.out.println(message.toLowerCase());
		greet(System.out::println);
		
	}
	
	public static void greet(Printer printer) {
		printer.print("Hello AU Bank!!");
	}
	
	
	public static void main(String[] args) {
		show();
	}
}
