package in.aubank.lambdas;

import java.util.function.Predicate;

public class PredicateMain {
	public static void main(String[] args) {
		
		Predicate<String> lengthGreaterThan5=(word)->word.length()>5;
		
		System.out.println("Tom: "+lengthGreaterThan5.test("Tom"));
		System.out.println("Elizabeth: "+lengthGreaterThan5.test("Elizabeth"));
		
		
		Predicate<String> hasLeftCurlyBracePredicate=(word)->word.startsWith("{");
		Predicate<String> hasRightCurlyBracePredicate=(word)->word.startsWith("}");
		
		Predicate<String> hasBothCurlyBracePredicate=hasLeftCurlyBracePredicate.and(hasRightCurlyBracePredicate);
		
		System.out.println("{M:M} ="+hasBothCurlyBracePredicate.test("{Mumbai:MH}"));
		
		
		
	}
}
 