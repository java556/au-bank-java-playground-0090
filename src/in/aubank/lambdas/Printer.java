package in.aubank.lambdas;

public interface Printer {
	public void print(String message);
}
