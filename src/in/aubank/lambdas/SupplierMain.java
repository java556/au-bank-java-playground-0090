package in.aubank.lambdas;

import java.util.function.Supplier;

public class SupplierMain {
	public static void main(String[] args) {
		
		Supplier<Double> getRandomSupplier=()->Math.random();
		
		double random=getRandomSupplier.get();
		
		System.out.println("Random Number: "+random);
	
	}
}
