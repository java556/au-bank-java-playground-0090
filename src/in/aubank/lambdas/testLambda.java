package in.aubank.lambdas;

public class testLambda {
	public void display() {
		System.out.println("Hello");
	}
	
	//()->System.out.println("Hello");
	
	public int square(int number) {
		return number*number;
	}
	
	//number -> number*number
	
	
	public void add() {
		System.out.println(a+b);
	}
	
	//(a,b)->System.out.println(a+b);
	
	
	public int calculate(int a,int b, int c) {
		int d =a+b;
		int e =d-c;
		int f=d*e;
		return f;
	}
	
//	(a,b,c)->{
//		int d =a+b;
//		int e =d-c;
//		int f=d*e;
//		return f;
//	}
	
	
}
