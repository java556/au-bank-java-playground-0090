package in.aubank.nio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopyFile {
	public static void main(String[] args) {
		try {
			Path p= Paths.get("Dataset/poem.txt");
			
			if(Files.exists(p)) {
				System.out.println("File already exists");
			}
			else {
				Path donePath=Files.createFile(p);
				System.out.println("File is created: "+donePath.toString());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
