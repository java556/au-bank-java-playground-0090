package in.aubank.nio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadFile {
	public static void main(String[] args) {
		try {
			Path p= Paths.get("Dataset/poem.txt");
			
			List<String> dataList = Files.readAllLines(p);
			
			for(String s:dataList) {
				System.out.println(s);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
