package in.aubank.nio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class WriteFile {
	public static void main(String[] args) {
		try {
//			Path p= Paths.get("Dataset/newFile1.txt");
//			Path domePath=Files.createFile(p);
//			
//			String contentString = "Hey Coding Owls";
//			
//			Files.write(domePath, contentString.getBytes());
//			System.out.println("Data written as Byte array");
			
			Path p2 = Paths.get("Dataset/newFile2.txt");
			Path donePath2 = Files.createFile(p2);
			
			Path p3=Paths.get("Dataset/poem.txt");
			List<String> dataList=Files.readAllLines(p3);
			
			Files.write(donePath2, dataList);
			
			System.out.println("Data Written");
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
