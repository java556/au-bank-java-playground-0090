package in.aubank.streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class StreamsMain {
	public static void main(String[] args) {
		
		Movies m1 = new Movies("a", 10);
		Movies m2 = new Movies("b", 20);
		Movies m3 = new Movies("c", 30);
		Movies m4 = new Movies("d", 40);
		Movies m5 = new Movies("e", 50);
		
		List<Movies> movies=new ArrayList<>();
		
		Collections.addAll(movies, m3,m4,m5,m1,m2);
		
	//  Imperative	
		int count = 0;
		
		for(Movies m:movies) {
			if(m.getLikes()>20) {
				count++;
			}
		}
		
		System.out.println("movie count: "+count);
		
	//  Declarative(functional)
		
		Predicate<Movies> isPopular=movie->movie.getLikes()>20;
		
		long count1 = movies.stream().filter(movie-> movie.getLikes()>20).count();
		
		System.out.println("Movies count: "+count1);
		
		System.out.println("----------------------------");
		
		movies.stream().forEach(movie->System.out.println(movie.getTitle()));
		
		System.out.println("---------------slice-----------------");
		
		movies.stream().map(movie->movie.getTitle()).limit(3).forEach(System.out::println);
		
		movies.stream().map(Movies::getTitle).limit(3).forEach(System.out::println);
		
		movies.stream().map(movie->movie.getTitle()).skip(2).forEach(System.out::println);
		
//		movies.stream().takeWhile(movie->movie.getLikes()<20).forEach(movie->System.out.println(movie.getTitle()));
		
		System.out.println("-------------Sort------------------");
		
		movies.stream().sorted((mo1,mo2)->mo1.getTitle().compareTo(mo2.getTitle())).forEach(m->System.out.println(m.getTitle()));
		
		movies.stream().sorted(Comparator.comparing(Movies::getTitle)).forEach(m->System.out.println(m.getTitle()));
		
		System.out.println("--------------------reducer---------------");
		
		boolean anyPopularMovie=movies.stream().anyMatch(isPopular);
		
		System.out.println("Any: "+ anyPopularMovie);
		
		boolean allPopularMovie=movies.stream().allMatch(isPopular);
		
		System.out.println("All: "+ allPopularMovie);
		
		Optional<Movies> optional = movies.stream().min(Comparator.comparing(Movies::getLikes));
		
		if(optional.isPresent()) {
			System.out.println(optional.get());
		}
		
		int likesCount =movies.stream().map(movie->movie.getLikes()).reduce(0,(acc,value)->acc+value);
		
		System.out.println("Number of likes count: "+likesCount);
		
		movies.stream().map(Movies::getLikes).reduce(0,Integer::sum);
		
		
		
		
		
		
		
	}
}
